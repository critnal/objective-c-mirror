//
//  ScheduleTableViewController.m
//  Assemble
//
//  Created by Temmink on 29/10/12.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "ScheduleTableViewController.h"

@interface ScheduleTableViewController ()

@end

@implementation ScheduleTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refresh
{
    UIAlertView *testAlert = [[UIAlertView alloc] initWithTitle:@"Test" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [testAlert show];
}

@end
