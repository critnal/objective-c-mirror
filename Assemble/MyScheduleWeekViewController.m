//
//  MyScheduleDefault.m
//  Assemble!
//
//  Created by Lion User on 06/09/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import "MyScheduleWeekViewController.h"
#import "AppDelegate.h"
#import "MyScheduleDayViewController.h"

@implementation MyScheduleWeekViewController

UILabel *labelsArray[7][24];
int dayButtonPressed;
AppDelegate *appDelegate;
UIImage *overlayImage;
UIImageView *overlayImageView;






- (void)viewDidUnload {
    [super viewDidUnload];
}






- (void)viewWillAppear:(BOOL)animated {
    
    for (int day = 0; day < 7; day++) {
        int hourPixel = 0;
        
        for (int hour = 0; hour < 24; hour++) {
            
            if ((hour%3 == 0) && (hour > 1)) hourPixel++;
            if ((hour%6 == 0) && (hour > 1)) hourPixel++;
            
            labelsArray[day][hour] = [[UILabel alloc] initWithFrame:CGRectMake(11 + 43 * day, 34 + 13 * hour + hourPixel, 41, 13)];
            labelsArray[day][hour].backgroundColor = [UIColor orangeColor];
            
            labelsArray[day][hour].alpha = 0.0;
            
            [self.labelsView addSubview:labelsArray[day][hour]];
        }
    }
    
    
    
    
    int indexValue = 0;
            
    for (int day = 0; day < 7; day++) {
        
        for (int hour = 0; hour < 24; hour++) {
            
            indexValue = [[[[appDelegate.appUser valueForKey:@"schedule"] objectAtIndex:day] objectAtIndex:hour] intValue];
                        
            if (indexValue == 1) {
                labelsArray[day][hour].alpha = 1;
            }
            
            else            
                labelsArray[day][hour].alpha = 0;
        }
    }
    
    [super viewWillAppear:animated];

}

- (void)viewDidDisappear:(BOOL)animated {
    UIView *subview;
    while ((subview = [[self.labelsView subviews] lastObject]) != nil)
        [subview removeFromSuperview];
}





- (IBAction)button0Pressed:(id)sender {
    dayButtonPressed = 0;
}
- (IBAction)button1Pressed:(id)sender {
    dayButtonPressed = 1;
}
- (IBAction)button2Pressed:(id)sender {
    dayButtonPressed = 2;
}
- (IBAction)button3Pressed:(id)sender {
    dayButtonPressed = 3;
}
- (IBAction)button4Pressed:(id)sender {
    dayButtonPressed = 4;
}
- (IBAction)button5Pressed:(id)sender {
    dayButtonPressed = 5;
}
- (IBAction)button6Pressed:(id)sender {
    dayButtonPressed = 6;
}





- (void)viewDidLoad {
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    overlayImage = [UIImage imageNamed:@"calendar_view_new_2.png"];
    overlayImageView = [[UIImageView alloc] initWithImage:overlayImage];
    overlayImageView.frame = self.labelsView.frame;
    
    [self.overlayView addSubview:overlayImageView];
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // This just makes the back button on the day view say "Week View" instead of "My Schedule"
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButton];
    
    // Tell the day view controller which day was pressed
    MyScheduleDayViewController *destinationController = segue.destinationViewController;
    destinationController.dayIndex = dayButtonPressed;
}

@end
